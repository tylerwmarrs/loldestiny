package service

import (
	"gopkg.in/mgo.v2/bson"
	_ "log"
	m "loldestiny/model"
	r "loldestiny/repository"
)

type WinPredictorService struct {
	GamePredictionRepository r.GamePredictionRepository
}

func (wp *WinPredictorService) GetStats() (*m.WinPredictorStats, error) {
	stats := new(m.WinPredictorStats)
	var err error
	stats.Observed, err = wp.GamePredictionRepository.Count(nil)
	stats.Validated, err = wp.GamePredictionRepository.Count(bson.M{"validated": true})
	stats.Correct, err = wp.GamePredictionRepository.Count(bson.M{"validated": true, "correct": true})
	stats.Incorrect, err = wp.GamePredictionRepository.Count(bson.M{"validated": true, "correct": false})
	stats.Accuracy = float64(stats.Correct) / float64(stats.Validated)

	if err != nil {
		return nil, err
	}

	return stats, err
}
