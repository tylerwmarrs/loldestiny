package service

import (
	_ "log"
	"sort"

	"lolapi"
)

type RegionService struct {
	LolApi *lolapi.LolApi
}

func (r *RegionService) GetAllRegionCodes() []string {
	regions := make([]string, 0)
	for _, region := range r.LolApi.AllDynamicRegion() {
		regions = append(regions, region.Code())
	}
	sort.Strings(regions)

	return regions
}
