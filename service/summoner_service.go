package service

import (
	"errors"
	"fmt"
	"log"
	"time"

	"lolapi"
	m "loldestiny/model"
	r "loldestiny/repository"
	u "loldestiny/util"
	cf "lollearn/classifiers"
)

type SummonerService struct {
	GamePredictionRepository r.GamePredictionRepository
	LolApi                   *lolapi.LolApi
}

func (s *SummonerService) SummonerWin(summonerName string, region string) (*m.SummonerWin, error) {
	s.LolApi.SetRegion(region)

	// look up summoner
	summoner, err := s.LolApi.SummonerByName(summonerName)
	if err != nil {
		log.Println("Unable to find summoner")
		return nil, errors.New(fmt.Sprintf("Unable to find summoner %s.", summonerName))
	}

	// look for current game by summoner ID
	currentGame, err := s.LolApi.SummonerCurrentGame(fmt.Sprintf("%d", summoner.ID))
	if err != nil {
		if err.StatusCode == 404 {
			log.Println("Summoner not in game.")
			return nil, errors.New(fmt.Sprintf("%s is not currently in a game.", summoner.Name))
		} else {
			log.Println(fmt.Sprintf("Unable to query current game: %d", err.StatusCode))
			return nil, errors.New("Unable to query for current game.")
		}
	}

	// logic only works for ranked queues at the moment
	if !currentGame.GameQueueID.IsRanked() {
		return nil, errors.New("Current game is not ranked.")
	}

	// TODO: cache static data...
	staticChamps, err := s.LolApi.StaticAllChampions()
	if err != nil {
		log.Println("Unable to query static champs")
		return nil, errors.New("Unable to query for static champions.")
	}

	outcome := new(m.SummonerWin)
	blueTeam := m.Team{}
	redTeam := m.Team{}
	blueWinRate := 0.0
	redWinRate := 0.0

	for _, participant := range currentGame.Participants {
		player := m.Player{}
		player.SummonerID = participant.SummonerID
		player.SummonerName = participant.SummonerName
		player.ChampionID = participant.ChampionID

		champion := u.ChampForId(player.ChampionID, staticChamps)
		player.ChampionName = champion.Name
		player.ChampionImage = champion.Image.Full
		if player.SummonerID == summoner.ID {
			player.Requestor = true
		}

		playerRankedStats := u.GetPlayerRankedStats(s.LolApi, player.SummonerID)
		champWinRate := u.GetChampionWinRate(participant.ChampionID, playerRankedStats)

		if participant.TeamID.IsBlueTeam() {
			blueWinRate += champWinRate
			blueTeam.AddPlayer(player)

			if player.SummonerID == summoner.ID {
				outcome.SummonerTeam = "blue"
			}
		} else {
			redWinRate += champWinRate
			redTeam.AddPlayer(player)

			if player.SummonerID == summoner.ID {
				outcome.SummonerTeam = "red"
			}
		}

		// TODO merge static data with player... champion data, spell data etc.
		// TODO: figure out better method than sleeping :/
		// probably not possible until I get production API key
		time.Sleep(time.Second)
	}

	outcome.BlueTeam = blueTeam
	outcome.RedTeam = redTeam
	outcome.Prediction = cf.CWRWinningTeamClassifier(blueWinRate, redWinRate)
	outcome.SummonerWin = outcome.SummonerTeam == outcome.Prediction

	// save record to mongo
	gp := m.GamePrediction{}
	gp.GameID = currentGame.GameID
	gp.RegionCode = region
	gp.BlueWinRate = blueWinRate
	gp.RedWinRate = redWinRate
	gp.Prediction = outcome.Prediction

	saveErr := s.GamePredictionRepository.SaveIfNotExist(gp)
	if saveErr != nil {
		log.Println("Unable to save prediction")
	}

	return outcome, nil
}
