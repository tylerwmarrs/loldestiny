package util

import (
	"fmt"
	"lolapi"
)

func ChampForId(id int, champs *lolapi.AllChampionsDto) *lolapi.ChampionDto {
	var c *lolapi.ChampionDto
	for _, champ := range champs.Champions {
		if champ.ID == id {
			c = champ
			break
		}
	}
	return c
}

func ChampNameForId(id int, champs *lolapi.AllChampionsDto) string {
	name := ""
	for _, champ := range champs.Champions {
		if champ.ID == id {
			name = champ.Name
			break
		}
	}
	return name
}

func GetPlayerRankedStats(api *lolapi.LolApi, id uint64) *lolapi.RankedStatsDto {
	rankedStats, err := api.SummonerStatsRanked(fmt.Sprintf("%d", id))
	if err == nil {
		return rankedStats
	}

	return nil
}

func GetChampionWinRate(champId int, rankStats *lolapi.RankedStatsDto) float64 {
	winRate := 50.0
	if rankStats == nil {
		return winRate
	}

	for _, champ := range rankStats.ChampionStats {
		if champ.ChampionID == champId {
			if champ.Stats.TotalSessionsPlayed == 0 {
				winRate = 50.0
			} else {
				winRate = champ.Stats.WinRate()
			}
		}
	}
	return winRate
}
