package main

import (
	"flag"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"lolapi"
	"loldestiny/config"
	"loldestiny/db/mongo"
	"loldestiny/repository"
	"os"
	"time"
)

// This is a command line utility that is used to validate whether or not
// a prediction from the winpredictor was correct or not. We query the
// mongo database for games that have not been validated, make API
// requests to the LoL API and update our database records accordingly.
func main() {
	configFile := flag.String("config", "", "Path to the config file")
	flag.Parse()
	if *configFile == "" {
		fmt.Println("Config file is required!")
		os.Exit(1)
	}

	appConfig, err := config.AppConfigFromFile(*configFile)
	if err != nil {
		fmt.Println("Unable to parse config file:", *configFile)
		os.Exit(1)
	}

	db := mongo.Mongo{}
	db.ConnectionURIs = appConfig.DB.Mongo.ConnectionURIs
	db.DB = appConfig.DB.Mongo.DB

	rep := repository.GamePredictionRepository{db}
	predictions, err := rep.Find(bson.M{"validated": false})
	if err != nil {
		fmt.Println("Unable to fetch unvalidated records...exiting")
		os.Exit(1)
	}

	api := lolapi.NewLolApi(appConfig.LolKey, "na")
	count := 0
	for _, p := range predictions {
		api.SetRegion(p.RegionCode)
		match, rerr := api.Match(fmt.Sprintf("%d", p.GameID), false)
		if rerr != nil {
			fmt.Println(fmt.Sprintf("Unable to validate game: %d, got %d",
				rerr.StatusCode, p.GameID))
			if rerr.StatusCode == 404 {
				continue
			}
		}

		winner := "blue"
		if match.Teams[0].TeamID.IsRedTeam() {
			if match.Teams[0].Winner {
				winner = "red"
			} else {
				winner = "blue"
			}
		} else {
			if match.Teams[0].Winner {
				winner = "blue"
			} else {
				winner = "red"
			}
		}

		if p.Prediction == winner {
			p.Correct = true
		}
		p.Validated = true
		err = rep.Save(p)
		if err != nil {
			fmt.Println(fmt.Sprintf("Unable to save validated prediction, game: %d", p.GameID))
		}

		count++
		time.Sleep(time.Second)
	}

	fmt.Println(fmt.Sprintf("Processed %d game(s)", count))
}
