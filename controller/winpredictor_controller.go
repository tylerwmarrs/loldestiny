package controller

import (
	"github.com/gin-gonic/gin"
	"loldestiny/service"
)

// Handles API requests for win predictor information.
type WinPredictorController struct {
	WinPredictorService service.WinPredictorService
}

// Provides web interface to obtain all region codes.
func (wp *WinPredictorController) Stats(c *gin.Context) {
	stats, err := wp.WinPredictorService.GetStats()
	if err != nil {
		c.JSON(200, gin.H{"error": err.Error()})
	} else {
		c.JSON(200, stats)
	}
}
