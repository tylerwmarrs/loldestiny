package controller

import (
	"github.com/gin-gonic/gin"
	"loldestiny/service"
)

// Handles API requests for region information.
type RegionController struct {
	RegionService service.RegionService
}

// Provides web interface to obtain all region codes.
func (rc *RegionController) AllRegionCodes(c *gin.Context) {
	c.JSON(200, rc.RegionService.GetAllRegionCodes())
}
