package controller

import (
	"github.com/gin-gonic/gin"
	"loldestiny/service"
	"strings"
)

// Handles API requests for Summoner information.
type SummonerController struct {
	SummonerService service.SummonerService
}

// Provides web interface to the Summoner service that predicts if they will
// win the game or not.
func (sc *SummonerController) WinPredictor(c *gin.Context) {
	name := c.Param("name")
	region := strings.ToLower(c.Param("region"))
	outcome, err := sc.SummonerService.SummonerWin(name, region)
	if err != nil {
		c.JSON(200, gin.H{"error": err.Error()})
	} else {
		c.JSON(200, outcome)
	}
}
