package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/itsjamie/gin-cors"
	"lolapi"
	"loldestiny/config"
	"loldestiny/controller"
	"loldestiny/db/mongo"
	"loldestiny/repository"
	"loldestiny/service"
	"os"
	_ "strings"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://loldestiny.tylermarrs.com")
		//c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	}
}

func main() {
	// Parse command line args
	configFlag := flag.String("config", "", "configuration file")
	flag.Parse()

	// set up config path
	configPath := "config.json"
	if *configFlag != "" {
		configPath = *configFlag
	}

	fmt.Println(fmt.Sprintf("Config Path: %s", configPath))

	// Read config file
	appConfig, err := config.AppConfigFromFile(configPath)
	if err != nil {
		panic(err)
		os.Exit(1)
	}

	// Set up lol api
	api := lolapi.NewLolApi(appConfig.LolKey, "na")
	api.WarmCache()

	// set gin in debug, release or test mode
	switch appConfig.Mode {
	case "release":
		gin.SetMode(gin.ReleaseMode)
	case "test":
		gin.SetMode(gin.TestMode)
	default:
		gin.SetMode(gin.DebugMode)
	}

	// Creates a gin router with default middleware:
	// logger and recovery (crash-free) middleware
	router := gin.New()

	// Global middleware

	// logging
	if appConfig.LogFile != "" {
		logFile, err := appConfig.GetLogFile()
		if err != nil {
			panic(err)
			os.Exit(1)
		}

		defer logFile.Close()
		router.Use(gin.LoggerWithWriter(logFile))
	} else {
		router.Use(gin.Logger())
	}

	// recovery - never die on panic
	router.Use(gin.Recovery())
	router.Use(CORSMiddleware())

	// CORS configuration
	/*router.Use(cors.Middleware(cors.Config{
		Origins:         strings.Join(appConfig.Cors.Origins, ","),
		Methods:         strings.Join(appConfig.Cors.Methods, ","),
		RequestHeaders:  strings.Join(appConfig.Cors.RequestHeaders, ","),
		ExposedHeaders:  strings.Join(appConfig.Cors.ExposeHeaders, ","),
		MaxAge:          appConfig.Cors.MaxAge,
		Credentials:     appConfig.Cors.Credentials,
		ValidateHeaders: appConfig.Cors.ValidateHeaders,
	}))*/

	// Set up controllers, services etc
	mongodb := mongo.NewMongo(appConfig.DB.Mongo.ConnectionURIs,
		appConfig.DB.Mongo.DB)

	// set up repositories
	gp := repository.GamePredictionRepository{mongodb}

	// summoner
	ss := service.SummonerService{gp, api}
	sc := controller.SummonerController{ss}
	router.GET("/summoner/name/:name/:region/winpredictor", sc.WinPredictor)

	// region
	rs := service.RegionService{api}
	rc := controller.RegionController{rs}
	router.GET("/region", rc.AllRegionCodes)

	// win predictor
	wps := service.WinPredictorService{gp}
	wpc := controller.WinPredictorController{wps}
	router.GET("/winpredictor/stats", wpc.Stats)

	// run the server
	router.Run(fmt.Sprintf("%s:%d", appConfig.Server.Host,
		appConfig.Server.Port))
}
