package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"time"
)

type AppConfig struct {
	// debug, release, test
	Mode string `json:"mode"`

	// logfile path to write to
	LogFile string `json:"logfile"`

	// Key for Riot's League of Legends API
	LolKey string `json:"lol_key"`
	Server struct {
		Host string `json:"host"`
		Port int    `json:"port"`
	} `json:"server"`
	Cors struct {
		Origins         []string      `json:"origins"`
		Methods         []string      `json:"methods"`
		RequestHeaders  []string      `json:"requestHeaders"`
		ExposeHeaders   []string      `json:"exposeHeaders"`
		MaxAge          time.Duration `json:"maxage"`
		Credentials     bool          `json:"credentials"`
		ValidateHeaders bool          `json:"validateHeaders"`
	} `json:"cors"`
	DB struct {
		Mongo struct {
			DB             string   `json:"db"`
			ConnectionURIs []string `json:"connection_uris"`
		} `json:"mongo"`
	} `json:"db"`
}

func DefaultAppConfig() AppConfig {
	config := AppConfig{}
	config.Mode = "debug"
	config.Server.Port = 8080
	config.Cors.Origins = []string{"*"}
	config.Cors.Methods = []string{"GET"}
	config.Cors.RequestHeaders = []string{"Origin", "Authorization", "Content-Type"}
	config.Cors.MaxAge = 50 * time.Second
	config.Cors.Credentials = true
	config.Cors.ValidateHeaders = true
	config.DB.Mongo.ConnectionURIs = []string{"localhost"}
	config.DB.Mongo.DB = "loldestiny"
	return config
}

func AppConfigFromFile(filePath string) (AppConfig, error) {
	config := AppConfig{}
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return AppConfig{}, err
	}

	err = json.Unmarshal(content, &config)
	if err != nil {
		return AppConfig{}, err
	}

	config.Cors.MaxAge = config.Cors.MaxAge * time.Second

	return config, nil
}

func (c *AppConfig) GetLogFile() (*os.File, error) {
	return os.Create(c.LogFile)
}
