package model

type SummonerWin struct {
	BlueTeam     Team   `json:"blueTeam"`
	RedTeam      Team   `json:"redTeam"`
	Prediction   string `json:"prediction"`
	SummonerTeam string `json:"summonerTeam"`
	SummonerWin  bool   `json:"summonerWin"`
}
