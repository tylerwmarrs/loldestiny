package model

type Team struct {
	Players []Player `json:"players"`
}

func (team *Team) AddPlayer(p Player) {
	team.Players = append(team.Players, p)
}
