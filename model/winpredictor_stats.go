package model

type WinPredictorStats struct {
	Observed  int     `json:"observed"`
	Validated int     `json:"validated"`
	Correct   int     `json:"correct"`
	Incorrect int     `json:"incorrect"`
	Accuracy  float64 `json:"accuracy"`
}
