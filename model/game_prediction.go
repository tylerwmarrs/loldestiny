package model

import (
	"gopkg.in/mgo.v2/bson"
)

type GamePrediction struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	GameID      uint64        `bson:"gameId"`
	RegionCode  string        `bson:"regionCode"`
	BlueWinRate float64       `bson:"blueWinRate"`
	RedWinRate  float64       `bson:"redWinRate"`
	Prediction  string        `bson:"prediction"`
	Validated   bool          `bson:"validated"`
	Correct     bool          `bson:"correct"`
}
