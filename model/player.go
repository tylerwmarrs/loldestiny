package model

type Player struct {
	ChampionName  string `json:"championName"`
	ChampionID    int    `json:"championID"`
	ChampionImage string `json:"championImage"`
	SummonerName  string `json:"summonerName"`
	SummonerID    uint64 `json:"summonerId"`
	Requestor     bool   `json:"requestor"`
}
