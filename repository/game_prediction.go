package repository

import (
	"gopkg.in/mgo.v2/bson"
	"loldestiny/db/mongo"
	"loldestiny/model"
)

const collectionName string = "gamepredictions"

type GamePredictionRepository struct {
	Mongo mongo.Mongo
}

// Save creates a document instance in the database or it updates an
// exisiting document. Update is based on whether the document provided
// has an ID or not.
func (g *GamePredictionRepository) Save(gp model.GamePrediction) error {

	err := g.Mongo.Connect()
	if err != nil {
		return err
	}

	defer g.Mongo.Close()

	col := g.Mongo.GetCollection(collectionName)
	if !gp.ID.Valid() {
		err = col.Insert(gp)
	} else {
		err = col.UpdateId(gp.ID, gp)
	}

	return err
}

func (g *GamePredictionRepository) SaveIfNotExist(gp model.GamePrediction) error {
	int, err := g.Count(bson.M{"gameId": gp.GameID})
	if int == 0 {
		err = g.Save(gp)
	}

	return err
}

func (g *GamePredictionRepository) Count(filter bson.M) (int, error) {
	err := g.Mongo.Connect()
	if err != nil {
		return 0, err
	}

	defer g.Mongo.Close()

	col := g.Mongo.GetCollection(collectionName)
	count, err := col.Find(filter).Count()
	return count, err
}

func (g *GamePredictionRepository) Find(filter bson.M) ([]model.GamePrediction, error) {
	predictions := make([]model.GamePrediction, 0)
	err := g.Mongo.Connect()
	if err != nil {
		return predictions, err
	}

	defer g.Mongo.Close()

	col := g.Mongo.GetCollection(collectionName)
	err = col.Find(filter).All(&predictions)

	return predictions, err
}

func (g *GamePredictionRepository) FindOne(filter bson.M) (model.GamePrediction, error) {
	prediction := model.GamePrediction{}
	err := g.Mongo.Connect()
	if err != nil {
		return prediction, err
	}

	defer g.Mongo.Close()

	col := g.Mongo.GetCollection(collectionName)
	err = col.Find(filter).One(&prediction)

	return prediction, err
}
